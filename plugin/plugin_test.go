package plugin

import (
	"os"
	"testing"
)

func TestMatch(t *testing.T) {
	var tcs = []struct {
		Path string
		Want bool
	}{
		{"fixtures/rails5/Gemfile.lock", true},
		{"fixtures/spree/Gemfile.lock", false},
		{"fixtures/wrong-name/Gemfile-bad.lock", false},
	}

	for _, tc := range tcs {
		fi, err := os.Stat(tc.Path)
		if err != nil {
			t.Error(err)
			continue
		}
		got, err := Match(tc.Path, fi)
		if err != nil {
			t.Error(err)
			continue
		}
		if got != tc.Want {
			t.Errorf("Wrong result for %s: expecting %v but got %v", tc.Path, tc.Want, got)
		}
	}
}
