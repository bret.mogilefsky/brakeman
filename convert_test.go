package main

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/issue"
)

func TestConvert(t *testing.T) {
	in := `{
  "warnings": [
    {
      "warning_type": "Cross-Site Scripting",
      "warning_code": 107,
      "fingerprint": "e0636b950dd005468b5f9a0426ed50936e136f18477ca983cfc51b79e29f6463",
      "check_name": "SanitizeMethods",
      "message": "rails-html-sanitizer 1.0.3 is vulnerable (CVE-2018-3741). Upgrade to 1.0.4",
      "file": "Gemfile.lock",
      "line": 97,
      "link": "https://groups.google.com/d/msg/rubyonrails-security/tP7W3kLc5u4/uDy2Br7xBgAJ",
      "code": null,
      "render_path": null,
      "location": null,
      "user_input": null,
      "confidence": "Medium"
    },
    {
      "warning_type": "Command Injection",
      "warning_code": 14,
      "fingerprint": "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
      "check_name": "Execute",
      "message": "Possible command injection",
      "file": "app/controllers/application_controller.rb",
      "line": 5,
      "link": "https://groups.google.com/d/msg/rubyonrails-security/tP7W3kLc5u4/uDy2Br7xBgAJ",
      "code": "system(params[:cmd])",
      "render_path": null,
      "location": {
        "type": "method",
        "class": "ApplicationController",
        "method": "foo"
      },
      "user_input": "params[:cmd]",
      "confidence": "High"
    },
    {
      "warning_type": "SQL Injection",
      "warning_code": 0,
      "fingerprint": "a16b9f9e2342465933a444ffdc01de6dc51d10eeb27de907f341a3318a6e1069",
      "check_name": "SQL",
      "message": "Possible SQL injection (CVE-2018-1234)",
      "file": "app/models/application_record.rb",
      "line": 8,
      "link": "https://brakemanscanner.org/docs/warning_types/sql_injection/",
      "code": "where(\"#{user_input}\")",
      "render_path": null,
      "location": {
        "type": "method",
        "class": "ApplicationRecord",
        "method": "some_method"
      },
      "user_input": "user_input",
      "confidence": "Medium"
    }
  ]
}`
	r := strings.NewReader(in)
	want := []issue.Issue{
		{
			Tool:       "brakeman",
			Category:   issue.CategorySast,
			Name:       "Possible command injection",
			Message:    "Possible command injection",
			CompareKey: "09abf078636daa3bc6a3d49fba232774a5d9b7c454e1003e6ab12cd56938296a",
			Confidence: issue.LevelHigh,
			Location: issue.Location{
				Class:     "ApplicationController",
				Method:    "foo",
				File:      "x/app/controllers/application_controller.rb",
				LineStart: 5,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "brakeman_warning_code",
					Name:  "Brakeman Warning Code 14",
					Value: "14",
					URL:   "",
				},
			},
			Links: []issue.Link{
				{
					URL: "https://groups.google.com/d/msg/rubyonrails-security/tP7W3kLc5u4/uDy2Br7xBgAJ",
				},
			},
		},
		{
			Tool:       "brakeman",
			Category:   issue.CategorySast,
			Name:       "Possible SQL injection (CVE-2018-1234)",
			Message:    "Possible SQL injection (CVE-2018-1234)",
			CompareKey: "a16b9f9e2342465933a444ffdc01de6dc51d10eeb27de907f341a3318a6e1069",
			Confidence: issue.LevelMedium,
			Location: issue.Location{
				Class:     "ApplicationRecord",
				Method:    "some_method",
				File:      "x/app/models/application_record.rb",
				LineStart: 8,
			},
			Identifiers: []issue.Identifier{
				{
					Type:  "brakeman_warning_code",
					Name:  "Brakeman Warning Code 0",
					Value: "0",
					URL:   "https://brakemanscanner.org/docs/warning_types/sql_injection/",
				},
				{
					Type:  "cve",
					Name:  "CVE-2018-1234",
					Value: "CVE-2018-1234",
					URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234",
				},
			},
			Links: []issue.Link{},
		},
	}
	got, err := convert(r, "x")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
