FROM ruby:latest
RUN gem install brakeman -v 4.3.1
COPY /analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
